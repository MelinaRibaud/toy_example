load kpmf_param
  
  tic;
  [U, V, err_train, err_test] = kpmf_func(R,z_m,iter,num_feat,epsilon,3);
  
  time = toc;
  
  save kpmf_res U V err_train err_test time
  
  
  
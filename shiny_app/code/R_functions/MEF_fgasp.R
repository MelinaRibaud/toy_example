MEF_fgasp = function(obj_toy,Index_seq)
{
  
  testing_ppl_i=obj_toy$index_kstar ## tirage de 4 random people
  seq.data.chr1 = obj_toy$Y
  
  training_ppl= (1:ncol(seq.data.chr1))[-testing_ppl_i]
  
  
  num_testing=length(obj_toy$index_nstar)
  
  num_ppl_all=ncol(seq.data.chr1)
  num_ppl=length(training_ppl)
  
  start_site_j=1   ##starting site
  
  intsec=4 
  testing_site_index= obj_toy$index_nstar#seq(1:(num_testing))*intsec 
 
  ## suite de 4, 8, 12, ... jusqua 1e6
  
  
  index_all_sites=1: nrow(seq.data.chr1)##all sites
  training_site_index=index_all_sites[-testing_site_index]
  
  
  #####here I normalize the input to 0 to 1 because some are too large
  input_all_ori=Index_seq[index_all_sites]
  input_ori=input_all_ori[-testing_site_index]
  testing_input_ori=input_all_ori[testing_site_index]
  
  x1=min(input_all_ori)
  x2=max(input_all_ori)-min(input_all_ori)
  
  input_all=(input_all_ori-x1)/(x2)
  input=(input_ori-x1)/(x2)
  testing_input=(testing_input_ori-x1)/(x2)
  
  
  ##############construct the training and testing output
  output_ppl=as.matrix(seq.data.chr1[index_all_sites,-testing_ppl_i])  ##the rest 20 people, full 126 sites 
  if(length(obj_toy$index_kstar)==1){
    output_site=as.matrix(seq.data.chr1[index_all_sites,testing_ppl_i][-obj_toy$index_nstar])
  }else{
    output_site=as.matrix(seq.data.chr1[index_all_sites,testing_ppl_i][-obj_toy$index_nstar,])
  }
  ### testing person, the rest 101 sites
  testing_output=as.matrix(seq.data.chr1[testing_site_index,testing_ppl_i])
  output=as.matrix(seq.data.chr1[index_all_sites,][-obj_toy$index_nstar,] )
  
  
  objet_fastgasp = list(Y_k_N = output_ppl,
                        Y_K_n = output,
                        Y_kstar_nstar =  testing_output,
                        index_nstar = testing_site_index,
                        index_kstar = testing_ppl_i,
                        index_N_scale = input_all)
  
  return(objet_fastgasp)
}

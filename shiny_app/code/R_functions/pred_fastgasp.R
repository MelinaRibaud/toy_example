library(FastGaSP)

pred_fastgasp = function(objet,obj_toy)
{
  output = objet$Y_K_n
  input = objet$index_N_scale
  testing_output = objet$Y_kstar_nstar
  testing_site_index = objet$index_nstar
  output_ppl = objet$Y_k_N
  testing_ppl_i = objet$index_kstar
  input_all = objet$index_N_scale
  input = input_all[-testing_site_index]
  
  training_ppl= (1:ncol(output))[-testing_ppl_i]
  #####row mean
  rowMeans_t_output=rowMeans(t(output))
  
  ########all number including the sites that need to be interpolate
  num_obs_all=nrow(output_ppl)
  num_obs=nrow(output)
  
  num_ppl_all = ncol(output)
  
  ####here I include the places that need to be interpolate
  #delta_x=input[2:num_obs]-input[1:(num_obs-1)];
  
  C=(max(input)-min(input))/num_obs
  
  svd_output=svd(t(output)-rowMeans_t_output )
  
  
  
  #form the basis
  #i_d=24
  A=svd_output$u[,1:num_ppl_all]%*%diag(svd_output$d[1:num_ppl_all])/sqrt(num_obs) ## let's use this as basis
  
  A_output_t=solve(t(A)%*%A)%*%t(A)%*%(t(output)-rowMeans_t_output)
  
  num_obs=length(input)
  num_dim=dim(A)[2]
  
  
  
  t_testing_output_levels=t(testing_output)
  t_testing_output_levels[which(t(testing_output)>=0.5)]=1
  t_testing_output_levels[which(t(testing_output)<0.5)]=0
  
  
  
  
  
  ##############all outputs including NA
  output_dlm=matrix(NA,num_dim,num_obs_all)
  
  for(i_t in 1:num_dim){
    output_dlm[i_t,-testing_site_index]=A_output_t[i_t,]
  }
  
  #####################optimization
  ###two functions
  ##prior
  approx_ref_matern_5_2<-function(a,b,C,beta_i,eta_i ){
    t=C*beta_i+eta_i  ###JR prior
    a*log(t)-b*t
  }
  ##log posterior of a GP
  ##the log lik is coded in the FastGaSP package
  log_post<-function(param,object){
    log_lik_val=log_lik(param,object)
    beta=exp(param[1])
    eta=exp(param[2])  ####constraint
    a=1/2
    b=1
    logprior=approx_ref_matern_5_2(a,b,C,beta,  eta)
    log_lik_val+ logprior
  }
  
  beta_record=rep(0,num_dim)
  eta_record=rep(0,num_dim)
  val_record=rep(0,num_dim)
  sigma2_record=rep(0,num_dim)
  
  
  system.time(
    for(i_dlm in 1:num_dim){
      print(i_dlm)
      fgasp.model=fgasp(input,A_output_t[i_dlm,])
      
      tt_all <- optim(c( log(1/C),1 ),log_post,object=fgasp.model,lower=c(-10,-10),
                      method="L-BFGS-B",
                      control = list(fnscale=-1,maxit=30)) 
      
      #tt_all
      val_record[i_dlm]=tt_all$value
      
      beta_record[i_dlm]=exp(tt_all$par)[1]
      eta_record[i_dlm]=exp(tt_all$par)[2]
      
    }
  )
  
  predict_all_dlm=matrix(0,num_dim, num_obs_all)
  sd_all_dlm=matrix(0,num_dim, num_obs_all)
  
  min_eta_record=min(eta_record)
  
  system.time(
    for(i_dlm in 1:num_dim){
      print(i_dlm)
      fgasp.model=fgasp(input,A_output_t[i_dlm,])
      
      pred_fast=predict(param=log(c(beta_record[i_dlm],eta_record[i_dlm])),object=fgasp.model,
                        testing_input=input_all)
      
      
      #pred_fast=Kalman_smoother(log(c(beta_record[i_dlm],eta_record[i_dlm])), index_obs, t(A_output_t[i_dlm,]),delta_x_all,var_est_record[i_dlm])
      predict_all_dlm[i_dlm,]=pred_fast@mean
      sd_all_dlm[i_dlm,]=sqrt(pred_fast@var)
    }
  )
  
  pred_all_record=A%*%(predict_all_dlm)
  pred_testing_record=pred_all_record[,testing_site_index]
  
  index_ppl_testing=testing_ppl_i
  
  pred_testing_record_cond=matrix(0,length(testing_ppl_i),length(testing_site_index))
  var_testing_record_cond=matrix(0,length(testing_ppl_i),length(testing_site_index))
  
  for(i_test in 1:length(testing_site_index)){
    if(i_test%%10000==0){
      print(i_test)
    }
    #print(i_test)
    Sigma_hat=A%*%diag(sd_all_dlm[,testing_site_index[i_test]]^2)%*%t(A)
    useful_block=Sigma_hat[testing_ppl_i,-testing_ppl_i]%*%solve(Sigma_hat[-testing_ppl_i,-testing_ppl_i])
    
    pred_testing_record_cond[,i_test]=pred_testing_record[testing_ppl_i,i_test]+useful_block%*%(output_ppl[testing_site_index[i_test],]-rowMeans_t_output[-testing_ppl_i] -pred_testing_record[-testing_ppl_i,i_test])
    var_predict=Sigma_hat[testing_ppl_i,testing_ppl_i]-useful_block%*%Sigma_hat[-testing_ppl_i,testing_ppl_i]
    var_testing_record_cond[,i_test]=diag(var_predict)
    
  }
  
  pred_nonseparable=pred_testing_record_cond+rowMeans_t_output[testing_ppl_i]
  
  pred_nonseparable[which(pred_nonseparable<0)]=0
  pred_nonseparable[which(pred_nonseparable>1)]=1
  
  Y_tot = obj_toy$Y
  Y_tot[obj_toy$index_nstar,obj_toy$index_kstar] = t(pred_nonseparable)
  
  
  return(Y_tot)
}
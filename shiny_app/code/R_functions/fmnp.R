library(R.matlab)

fmnp = function(Y,
                z_m,
               iter,
               num_feat,
               epsilon,
               diffu,
               path = "code/matlab_obj/"){
  path_old = getwd()
  setwd(path)
  R = as.matrix(Y)
  names(R) = NULL
  writeMat("kpmf_param.mat",
           R = R,
           num_feat = num_feat,
           z_m = z_m,
           iter = iter,
           diffu = diffu,
           epsilon = epsilon)
  system("matlab -nodisplay -r \"run('kpmf.m');exit;\"")
  res_kpmf = readMat("kpmf_res.mat")
  setwd(path_old)
  return(res_kpmf)
}
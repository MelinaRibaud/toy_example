
library(ZIprop)
set_obj = function(Y, K, kstar, N, nstar){
  Y = as.matrix(Y)
  names(Y) = NULL
  index_nstar = round(seq(1,N,length.out = nstar))
  index_kstar = sample(1:K,kstar)
  obj_sim_toy = list(Y = Y, index_kstar = index_kstar, index_nstar = index_nstar)
  return(obj_sim_toy)
}